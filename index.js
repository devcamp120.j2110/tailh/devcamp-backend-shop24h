//Import Express
const express = require("express");
//Import Mongoose
const mongoose = require("mongoose");
// Import Routers
const productRouter = require("./src/routes/ProductRouter");
const customerRouter = require("./src/routes/CustomerRouter");
const orderRouter = require("./src/routes/OrderRouter");
const orderDetailRouter = require("./src/routes/OrderDetailRouter");
const app = express();
// Khai Báo Body Lấy tiếng việt
app.use(
  express.urlencoded({
    extend: true,
  })
);
// Khai báo body dạng JSON
app.use(express.json());
// CORS
app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});
// Kết nối mongoDB
async function connectMongoDB() {
  await mongoose.connect("mongodb://localhost:27017/CRUD_Shop24h");
}
// Thực thi kết nối
connectMongoDB()
  .then(() => {
    console.log("Connect MongoDB Successfully!");
  })
  .catch((err) => {
    console.log(err);
  });

app.get("/", (req, res) => {
  res.json({
    message: "CRUD Shop24h API",
  });
});
// Router
app.use("/products", productRouter);
app.use("/customers", customerRouter);
app.use("/orders", orderRouter);
app.use("/orderDetail", orderDetailRouter);
// Lắng nghe port
// Start server
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
});
