//Import Mongoose
const mongoose = require("mongoose");
//Import Model
const { OrderDetail } = require("../models/OrderDetail");
// Tạo OrderDetail
function createOrderDetail(req, res) {
  // Tạo OrderDetail từ Order Detail Model
  const orderDetail = new OrderDetail({
    _id: mongoose.Types.ObjectId(),
    order: req.body.order,
    product: req.body.product,
    quantity: req.body.quantity,
    priceEach: req.body.priceEach,
  });
  orderDetail
    .save()
    .then((newOrderDetail) => {
      return res.status(200).json({
        message: "Success",
        orderDetail: newOrderDetail,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}
// Lấy tất cả Order Detail từ OrderId
function getAllOrderDetailOfOrder(req, res) {
  const orderId = req.params.orderId;
  // Tìm theo OrderID
  OrderDetail.find({
    order: orderId,
  })
    .populate({ path: "product order" })
    .then((data) => {
      return res.status(200).json({
        message: "Success",
        orderDetails: data,
      });
    })
    .catch((error) => {
      return res.status(404).json({
        message: "Not Found",
        error: error.message,
      });
    });
}
//Lấy 1 OrderDetail
function getOneOrderDetail(req, res) {
  const orderDetailId = req.params.orderDetailId;
  // Kiểm tra customer ID có phải ObjectID hay không?!
  if (mongoose.Types.ObjectId.isValid(orderDetailId)) {
    OrderDetail.findById(orderDetailId)
      .then((data) => {
        if (data) {
          return res.status(200).json({
            message: "Success",
            orderDetail: data,
          });
        } else {
          return res.status(404).json({
            message: "Fail",
            error: "Not Found",
          });
        }
      })
      .catch((error) => {
        return res.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
    return res.status(400).json({
      message: "Fail",
      error: "OrderDetailId is not valid",
    });
  }
}
// Update Order Detail
function updateOrderDetail(req, res) {
  // Lấy order Detail ID từ paramUrl
  const orderDetailId = req.params.orderDetailId;
  //Lấy Body request
  const updateObj = req.body;

  //Kiểm tra xem orderDetailId có phải ObjectID hay không
  if (mongoose.Types.ObjectId.isValid(orderDetailId)) {
    OrderDetail.findByIdAndUpdate(orderDetailId, updateObj)
      .then((updatedObj) => {
        return res.status(200).json({
          message: "Success",
          updatedObj: updatedObj,
        });
      })
      .catch((error) => {
        return res.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
    return res.status(400).json({
      message: "Fail",
      error: "OrderDetailId is not valid",
    });
  }
}
// Delete Order Detail
function deleteOrderDetail(req, res) {
  // Lấy order id từ paramUrl
  const orderDetailId = req.params.orderDetailId;
  //Kiểm tra customerId có phải ObjectId hay không?!
  if (mongoose.Types.ObjectId.isValid(orderDetailId)) {
    OrderDetail.findByIdAndDelete(orderDetailId)
      .then(() => {
        return res.status(204).json({
          message: "Delete Success",
        });
      })
      .catch((error) => {
        return res.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
    return res.status(400).json({
      message: "Fail",
      error: "OrderDetailId is not valid",
    });
  }
}
// Export Controller
module.exports = {
  createOrderDetail,
  getAllOrderDetailOfOrder,
  getOneOrderDetail,
  updateOrderDetail,
  deleteOrderDetail,
};
