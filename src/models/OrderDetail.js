//Import Mongoose
const mongoose = require("mongoose"); 
//Lấy Schema từ mongoose
const { Schema } = mongoose; 
// Tạo OrderDetail Schema MongoDB
const orderDetailSchema = new Schema({
  _id: Schema.Types.ObjectId, //Trường _id có kiểu dữ liệu objectID
  order: {
    type: Schema.Types.ObjectId,
    ref: "Order",
  },
  product: {
    type: Schema.Types.ObjectId,
    ref: "Product",
  },
  quantity: {
    type: Number,
    require: true,
  },
  priceEach: {
    type: Number,
    require: true,
  },
});
//Tạo Model
const OrderDetail = mongoose.model("OrderDetail", orderDetailSchema);
// Export Product Model
module.exports = { OrderDetail };

