//Import Mongoose
const mongoose = require("mongoose"); 
//Import Model
const { Order } = require("../models/Order"); 
const { OrderDetail } = require("../models/OrderDetail");

// Tạo Order
function createOrder(req, res) {
  // Tạo Order từ OrderModel
  const order = new Order({
    _id: mongoose.Types.ObjectId(),
    customer: req.body.customer,
    note: req.body.note,
    shippedDate: req.body.shippedDate,
  });

  const orderDetails = req.body.orderDetails;
  orderDetails.forEach((element) => {
    const orderDetail = new OrderDetail({
      _id: mongoose.Types.ObjectId(),
      order: order._id,
      product: element.product._id,
      quantity: element.quantity,
      priceEach: element.product.promotionPrice * element.quantity,
    });
    orderDetail.save();
  });
  order
    .save()
    .then((newOrder) => {
      return res.status(200).json({
        message: "Success",
        order: newOrder,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}
// Lấy Tất Cả Order
function getAllOrder(req, res) {
  const { limit, skip } = req.query;
  Order.find()
    .populate({ path: "customer" })
    .limit(limit)
    .skip(skip)
    .select(
      "orderDate requiredDate shippedDate note status customer timeCreated timeUpdated"
    )
    .then((orders) => {
      return res.status(200).json({
        message: "Success",
        orders: orders,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}
// Get Count Customer
function getCountOrder(req, res) {
  Order.count()
    .then((number) => {
      return res.status(200).json({
        message: "Success",
        number: number,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}
// Lấy 1 Order
function getOneOrder(req, res) {
  // Lấy orderId từ params URL
  const orderId = req.params.orderId;

  // Kiểm tra orderId có phải Object ID hay không?
  if (mongoose.Types.ObjectId.isValid(orderId)) {
    Order.findById(orderId)
      .then((data) => {
        if (data) {
          return res.status(200).json({
            message: "Success",
            order: data,
          });
        } else {
          return res.status(404).json({
            message: "Fail",
            error: "Not Found",
          });
        }
      })
      .catch((error) => {
        return res.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
    return res.status(400).json({
      message: "Fail",
      error: "CustomerId is not valid",
    });
  }
}
// Get Order Filtered
function getOrderByCustomer(req, res) {
  const {limit , skip , customer } = req.query;

  let conditions = {};
  if (customer) {
    conditions.customer = customer;
  }

  Order.find(conditions)
    .populate({ path: "customer" })
    .limit(limit)
    .skip(skip)
    .select(
      "orderDate requiredDate shippedDate note status customer timeCreated timeUpdated"
    ) //Select các trường cần lấy
    .then((orders) => {
      return res.status(200).json({
        message: "Success",
        orders: orders,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}
// Update Order
function updateOrder(req, res) {
  // Lấy order id từ paramUrl
  const orderId = req.params.orderId;
  //Lấy Body request
  const updateObj = req.body;

  //Kiểm tra xem orderId có phải ObjectID hay không
  if (mongoose.Types.ObjectId.isValid(orderId)) {
    Order.findByIdAndUpdate(orderId, updateObj)
      .then((updatedObj) => {
        return res.status(200).json({
          message: "Success",
          updatedObj: updatedObj,
        });
      })
      .catch((error) => {
        return res.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
    return res.status(400).json({
      message: "Fail",
      error: "OrderId is not valid",
    });
  }
}
// Delete Order
function deleteOrder(req, res) {
  // Lấy order id từ paramUrl
  const orderId = req.params.orderId;

  //Kiểm tra customerId có phải ObjectId hay không?!
  if (mongoose.Types.ObjectId.isValid(orderId)) {
    Order.findByIdAndDelete(orderId)
      .then(() => {
        return res.status(204).json({
          message: "Delete Success",
        });
      })
      .catch((error) => {
        return res.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
    return res.status(400).json({
      message: "Fail",
      error: "OrderId is not valid",
    });
  }
}
module.exports = {
  createOrder,
  getAllOrder,
  getOneOrder,
  updateOrder,
  deleteOrder,
  getCountOrder,
  getOrderByCustomer,
};
