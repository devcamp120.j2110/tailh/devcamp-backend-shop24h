//Import Mongoose
const mongoose = require("mongoose"); 
//Lấy Schema từ mongoose
const { Schema } = mongoose; 

//Tạo Order Schema MongoDB
const orderSchema = new Schema({
  _id: Schema.Types.ObjectId, //Trường _id có kiểu dữ liệu objectID
  orderDate: {
    type: Date,
    default: Date.now,
  },
  requiredDate: {
    type: Date,
    default: Date.now,
  },
  shippedDate: {
    type: Date,
    default: undefined,
  },
  note: {
    type: String,
    default: "",
  },
  status: {
    type: Number,
    default: 0,
  },
  timeCreated: {
    type: Date,
    default: Date.now,
  },
  timeUpdated: {
    type: Date,
    default: Date.now,
  },
  customer: {
    type: Schema.Types.ObjectId,
    ref: "Customer",
    require: true,
  },
});

// Tạo Model
const Order = mongoose.model("Order", orderSchema);

// Export Customer Model
module.exports = { Order };
