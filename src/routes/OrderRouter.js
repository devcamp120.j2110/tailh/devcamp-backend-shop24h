//Import Express
const express = require("express"); 
// Router
const router = express.Router();
// Import Order Controller
const {
  createOrder,
  getAllOrder,
  getOneOrder,
  updateOrder,
  deleteOrder,
  getCountOrder,
  getOrderByCustomer
} = require("../controllers/OrderController");
// POST
router.post("/", createOrder);
// GET
router.get("/", getAllOrder);
router.get("/count", getCountOrder);
router.get("/filter", getOrderByCustomer);
router.get("/:orderId", getOneOrder);
// PUT
router.put("/:orderId", updateOrder);
// Delete
router.delete("/:orderId", deleteOrder);
// Export Routers
module.exports = router;
