//Import Mongoose
const mongoose = require("mongoose"); 
//Lấy Schema từ mongoose
const { Schema } = mongoose; 

//Tạo Customer Schema MongoDB
const customerSchema = new Schema({
  _id: Schema.Types.ObjectId, //Trường _id có kiểu dữ liệu objectID
  role: {
    type: String,
    default: "user",
  },
  fullname: {
    type: String,
    require: true,
  },
  password: {
    type: String,
    default: "a1234567",
  },
  photoURL: {
    type: String,
    default: "https://images.unsplash.com/photo-1635492491273-455af7728453?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=960&q=80",
  },
  phoneNumber: {
    type: String,
    require: true,
  },
  email: {
    type: String,
    require: true,
    unique: true,
  },
  address: {
    type: String,
    require: true,
  },
  city: {
    type: String,
    require: true,
  },
  country: {
    type: String,
    require: true,
  },
  timeCreated: {
    type: Date,
    default: Date.now,
  },
  timeUpdated: {
    type: Date,
    default: Date.now,
  },
});

// Tạo Model
const Customer = mongoose.model("Customer", customerSchema);

// Export Customer Model
module.exports = { Customer };
