//Import Mongoose
const mongoose = require("mongoose"); 
//Import Model
const { Product } = require("../models/Product"); 
// Tạo Mới 1 Sản Phẩm
function createProduct(req, res) {
  // Tạo obj từ Product Model
  const product = new Product({
    _id: mongoose.Types.ObjectId(),
    name: req.body.name,
    type: req.body.type,
    color: req.body.color,
    imageUrl: req.body.imageUrl,
    buyPrice: req.body.buyPrice,
    promotionPrice: req.body.promotionPrice,
    description: req.body.description,
    thumbnailUrl: req.body.thumbnailUrl,
  });
  product
    .save()
    .then((newProduct) => {
      return res.status(200).json({
        message: "Success",
        product: newProduct,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}
// Get All Product
function getAllProduct(req, res) {
  const { limit, skip } = req.query;
  Product.find()
    .limit(limit)
    .skip(skip)
    .select(
      "_id name type imageUrl buyPrice promotionPrice description timeCreated timeUpdated thumbnailUrl color"
    ) //Select các trường cần lấy
    .then((products) => {
      return res.status(200).json({
        message: "Success",
        products: products,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}
// Get Product By Product Name
function getProductByProductName(req,res) {
  const name = req.params.name;
  let conditions = {};
  if (name) {
    conditions.name = name;
  }
  Product.findOne(conditions)
  .select(
    "_id name type imageUrl buyPrice promotionPrice description timeCreated timeUpdated thumbnailUrl color"
  ) //Select các trường cần lấy
  .then((product) => {
    return res.status(200).json({
      message: "Success",
      product: product,
    });
  })
  .catch((error) => {
    return res.status(500).json({
      message: "Fail",
      error: error.message,
    });
  });
}
// Get Product Filtered
function getProductFiltered(req, res) {
  const { limit, skip, name, type, color, buyPrice, minPrice, maxPrice } =
    req.query;
  let conditions = {};
  if (name) {
    let nameRegex = new RegExp(`${name}`);
    conditions.name = nameRegex;
  }
  if (type) {
    conditions.type = type;
  }
  if (color) {
    conditions.color = color;
  }
  if (minPrice && maxPrice) {
    if (minPrice !== "" && maxPrice === "") {
      conditions.promotionPrice = { $gte: minPrice };
    }
    if (maxPrice !== "" && minPrice === "") {
      conditions.promotionPrice = { $lte: maxPrice };
    }
    if (minPrice !== "" && maxPrice !== "") {
      conditions.promotionPrice = { $lte: maxPrice, $gte: minPrice };
    }
  }

  if (buyPrice) {
    if (buyPrice === "low") {
      conditions.buyPrice = { $lte: 5000 };
    } else if (buyPrice === "high") {
      conditions.buyPrice = { $gte: 5000 };
    }
  }
  console.log(conditions);
  Product.find(conditions)
    .limit(limit)
    .skip(skip)
    .select(
      "_id name type imageUrl buyPrice promotionPrice description timeCreated timeUpdated thumbnailUrl color"
    ) //Select các trường cần lấy
    .then((products) => {
      return res.status(200).json({
        message: "Success",
        products: products,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}
// Get Product Related
function getProductRelated(req, res) {
  const type = req.query.type;
  let conditions = {};
  if (type) {
    conditions.type = type;
  }
  Product.find(conditions)
    .select(
      "_id name type imageUrl buyPrice promotionPrice description timeCreated timeUpdated thumbnailUrl color"
    ) //Select các trường cần lấy
    .then((products) => {
      return res.status(200).json({
        message: "Success",
        products: products,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}
// Get Count Product
function getCountProducts(req, res) {
  Product.count()
    .then((number) => {
      return res.status(200).json({
        message: "Success",
        number: number,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}
// Get Product By ID
function getProductByID(req, res) {
  // Lấy product id từ URL
  const productId = req.params.productId;
  //Kiểm tra productId có phải ObjectId hay không?!
  if (mongoose.Types.ObjectId.isValid(productId)) {
    Product.findById(productId)
      .then((data) => {
        if (data) {
          return res.status(200).json({
            message: "Success",
            product: data,
          });
        } else {
          return res.status(404).json({
            message: "Fail",
            error: "Not found",
          });
        }
      })
      .catch((error) => {
        return res.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    // Nếu không phải ObjectID
    return res.status(400).json({
      //Trả ra lỗi 400 - Bad Request
      message: "Fail",
      error: "ProductId is not valid!",
    });
  }
}
// Update Product
function updateProduct(req, res) {
  // Lấy product id từ URL
  const productId = req.params.productId;
  // Lấy obj request
  const updateObject = req.body;
  //Kiểm tra productId có phải ObjectId hay không?!
  if (mongoose.Types.ObjectId.isValid(productId)) {
    Product.findByIdAndUpdate(productId, updateObject)
      .then((updatedProduct) => {
        return res.status(200).json({
          message: "Success",
          updatedProduct: updatedProduct,
        });
      })
      .catch((error) => {
        return res.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
    return res.status(400).json({
      message: "Fail",
      error: "UserID is not valid",
    });
  }
}
// Delete Product
function deleteProduct(req, res) {
  // Lấy product id từ URL
  const productId = req.params.productId;
  //Kiểm tra productId có phải ObjectId hay không?!
  if (mongoose.Types.ObjectId.isValid(productId)) {
    Product.findByIdAndDelete(productId)
      .then(() => {
        return res.status(204).json({
          message: "Delete Success",
        });
      })
      .catch((error) => {
        return res.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
    return res.status(400).json({
      message: "Fail",
      error: "ProductId is not valid",
    });
  }
}
// Export Controller
module.exports = {
  createProduct,
  getAllProduct,
  getProductByID,
  getCountProducts,
  getProductFiltered,
  getProductRelated,
  getProductByProductName,
  updateProduct,
  deleteProduct,
};
