//Import Express
const express = require("express"); 
// Router
const router = express.Router();
// Import Customer Controller
const {
  createCustomer,
  getAllCustomer,
  getOneCustomer,
  updateCustomer,
  deleteCustomer,
  getOneCustomerByEmail,
  getOneCustomerByEmailAndPassword,
  getCountCustomer,
  getCustomerFiltered
} = require("../controllers/CustomerController");

// Post
router.post("/", createCustomer);
// Get
router.get("/", getAllCustomer);
router.get("/email/:email", getOneCustomerByEmail);
router.get("/email-password", getOneCustomerByEmailAndPassword);
router.get("/count", getCountCustomer);
router.get("/customer-filter", getCustomerFiltered);
router.get("/:customerId", getOneCustomer);
// PUT
router.put("/:customerId", updateCustomer);
// Delete
router.delete("/:customerId", deleteCustomer);

// Export Routers
module.exports = router;
