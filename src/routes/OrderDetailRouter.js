//Import Express
const express = require("express"); 
// Router
const router = express.Router();
// Import OrderDetail Controller
const {
    createOrderDetail ,
    getAllOrderDetailOfOrder,
    getOneOrderDetail,
    updateOrderDetail,
    deleteOrderDetail
} = require("../controllers/OrderDetailController");
// Post
router.post("/", createOrderDetail);
// Get
router.get("/:orderId", getAllOrderDetailOfOrder);
router.get("/oneOrderDetail/:orderDetailId", getOneOrderDetail);
// PUT
router.put("/oneOrderDetail/:orderDetailId", updateOrderDetail);
// Delete
router.delete("/oneOrderDetail/:orderDetailId", deleteOrderDetail);
// Export Routers
module.exports = router;
