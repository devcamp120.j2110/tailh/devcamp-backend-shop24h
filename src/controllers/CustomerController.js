//Import Mongoose
const mongoose = require("mongoose"); 
//Import Model
const { Customer } = require("../models/Customer"); 

// Tạo mới 1 Customer
function createCustomer(req, res) {
  // Tạo obj từ Customer Model
  const customer = new Customer({
    _id: mongoose.Types.ObjectId(),
    role: req.body.role,
    fullname: req.body.fullname,
    password: req.body.password,
    photoURL: req.body.photoURL,
    phoneNumber: req.body.phoneNumber,
    email: req.body.email,
    address: req.body.address,
    city: req.body.city,
    country: req.body.country,
  });

  customer
    .save()
    .then((newCustomer) => {
      return res.status(200).json({
        message: "Success",
        customer: newCustomer,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: "Fail",
        error: error.message,
        email: customer.email,
      });
    });
}
// Lấy Tất Cả Customer
function getAllCustomer(req, res) {
  const { limit, skip } = req.query;
  Customer.find()
    .limit(limit)
    .skip(skip)
    .select(
      "role fullname phoneNumber email photoURL address password city country timeCreated timeUpdated"
    )
    .then((customers) => {
      return res.status(200).json({
        message: "Success",
        customers: customers,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}
// Get Count Customer
function getCountCustomer(req, res) {
  Customer.count()
    .then((number) => {
      return res.status(200).json({
        message: "Success",
        number: number,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}
// Lấy 1 Customer từ 1 ID
function getOneCustomer(req, res) {
  // Lấy customer id từ paramUrl
  const customerId = req.params.customerId;
  // Kiểm tra customer ID có phải ObjectID hay không?!
  if (mongoose.Types.ObjectId.isValid(customerId)) {
    Customer.findById(customerId)
      .then((data) => {
        if (data) {
          return res.status(200).json({
            message: "Success",
            customer: data,
          });
        } else {
          return res.status(404).json({
            message: "Fail",
            error: "Not Found",
          });
        }
      })
      .catch((error) => {
        return res.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
    return res.status(400).json({
      message: "Fail",
      error: "CustomerId is not valid",
    });
  }
}
// Get Customer Filtered
function getCustomerFiltered(req, res) {
  const { limit, skip, fullname, email, phoneNumber } = req.query;

  let conditions = {};
  if (fullname) {
    let fullnameRegex = new RegExp(`${fullname}`);
    conditions.fullname = fullnameRegex;
  }
  if (email) {
    let emailRegex = new RegExp(`${email}`);
    conditions.email = emailRegex;
  }
  if (phoneNumber) {
    let phoneNumberRegex = new RegExp(`${phoneNumber}`);
    conditions.phoneNumber = phoneNumberRegex;
  }

  Customer.find(conditions)
    .limit(limit)
    .skip(skip)
    .select(
      "role fullname phoneNumber email photoURL address password city country timeCreated timeUpdated"
    ) //Select các trường cần lấy
    .then((customers) => {
      return res.status(200).json({
        message: "Success",
        customers: customers,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}
// Lấy 1 Customer từ 1 Email
function getOneCustomerByEmail(req, res) {
  const email = req.params.email;
  let conditions = {};
  if (email) {
    conditions.email = email;
  }
  Customer.findOne(conditions)
    .select(
      "role fullname phoneNumber email photoURL address password city country timeCreated timeUpdated"
    ) //Select các trường cần lấy
    .then((customer) => {
      return res.status(200).json({
        message: "Success",
        customer: customer,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}
// Lấy 1 Customer từ Email và Password
function getOneCustomerByEmailAndPassword(req, res) {
  const { email, password } = req.query;
  let conditions = {};
  if (email) {
    conditions.email = email;
  }
  if (password) {
    conditions.password = password;
  }
  Customer.findOne(conditions)
    .select(
      "role fullname phoneNumber email photoURL address password city country timeCreated timeUpdated"
    ) //Select các trường cần lấy
    .then((customer) => {
      return res.status(200).json({
        message: "Success",
        customer: customer,
      });
    })
    .catch((error) => {
      return res.status(500).json({
        message: "Fail",
        error: error.message,
      });
    });
}
// Update Customer
function updateCustomer(req, res) {
  // Lấy customer id từ paramUrl
  const customerId = req.params.customerId;
  //Lấy Body request
  const updateObj = req.body;

  //Kiểm tra xem customerId có phải ObjectID hay không
  if (mongoose.Types.ObjectId.isValid(customerId)) {
    Customer.findByIdAndUpdate(customerId, updateObj)
      .then((updatedObj) => {
        return res.status(200).json({
          message: "Success",
          updatedObj: updatedObj,
        });
      })
      .catch((error) => {
        return res.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
    return res.status(400).json({
      message: "Fail",
      error: "CustomerId is not valid",
    });
  }
}
// Delete Customer
function deleteCustomer(req, res) {
  // Lấy customerId từ URL
  const customerId = req.params.customerId;
  //Kiểm tra customerId có phải ObjectId hay không?!
  if (mongoose.Types.ObjectId.isValid(customerId)) {
    Customer.findByIdAndDelete(customerId)
      .then(() => {
        return res.status(204).json({
          message: "Delete Success",
        });
      })
      .catch((error) => {
        return res.status(500).json({
          message: "Fail",
          error: error.message,
        });
      });
  } else {
    // Nếu không phải ObjectID thì trả ra lỗi 400 - Bad request
    return res.status(400).json({
      message: "Fail",
      error: "CustomerId is not valid",
    });
  }
}
// Export Control
module.exports = {
  createCustomer,
  getAllCustomer,
  getOneCustomer,
  updateCustomer,
  deleteCustomer,
  getOneCustomerByEmail,
  getOneCustomerByEmailAndPassword,
  getCountCustomer,
  getCustomerFiltered,
};
