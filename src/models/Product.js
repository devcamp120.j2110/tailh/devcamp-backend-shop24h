//Import Mongoose
const mongoose = require("mongoose");
//Lấy Schema từ mongoose
const { Schema } = mongoose;
// Tạo Product Schema mongoDB
const productSchema = new Schema({
  _id: Schema.Types.ObjectId, //Trường _id có kiểu dữ liệu objectID
  name: {
    type: String,
    require: true,
    unique: true,
  },
  type: {
    type: String,
    require: true,
  },
  color: {
    type: String,
    require: true,
  },
  imageUrl: {
    type: String,
    require: true,
  },
  buyPrice: {
    type: Number,
    require: true,
  },
  promotionPrice: {
    type: Number,
    require: true,
  },
  description: {
    type: String,
    require: true,
  },
  thumbnailUrl: {
    type: Array,
    require: true,
  },
  timeCreated: {
    type: Date,
    default: Date.now,
  },
  timeUpdated: {
    type: Date,
    default: Date.now,
  },
});
//Tạo Model
const Product = mongoose.model("Product", productSchema);
// Export Product Model
module.exports = { Product };
