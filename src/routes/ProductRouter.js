//Import Express
const express = require("express");
// Router
const router = express.Router();
// Import Product controller
const {
  createProduct,
  getAllProduct,
  getProductByID,
  getCountProducts,
  getProductFiltered,
  getProductRelated,
  updateProduct,
  deleteProduct,
  getProductByProductName,
} = require("../controllers/ProductController");
// POST
router.post("/", createProduct);
// GET
router.get("/", getAllProduct);
router.get("/filter", getProductFiltered);
router.get("/related", getProductRelated);
router.get("/name/:name", getProductByProductName);
router.get("/count", getCountProducts);
router.get("/:productId", getProductByID);
// PUT
router.put("/:productId", updateProduct);
// Delete
router.delete("/:productId", deleteProduct);
//Export routers
module.exports = router;
